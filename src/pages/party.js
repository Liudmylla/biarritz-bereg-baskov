
import React from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
export default () => (

    <Layout>
        <SEO
            title="Мероприятия"
            keywords={['биарриц', 'Англет', 'страна Басков', 'отдых в Биаррице', 'гид в Биаррице', 'серфинг', 'гольф', 'экскурсии в Биаррице', 'аренда виллы в Биаррице', 'недвижимость в Биаррице']}
        />
        <div className="service section" id="Service">
            <div className="container">
                <div className="container-service">
                    

                    <div className="row">
                        <div className="col-md-12  ">
                            <div className="service-main">
                                <h2>Организация мероприятий :</h2>
                            </div>
                            <div className="service-main">
                                <img
                                    src="https://images.ctfassets.net/cr97yccuaymg/386x2qlIwuTgKK4F0fFRB0/4a6432d5bac472fd03a252c39950ff88/bbb8.jpeg?w=600&h=245&q=50"
                                    alt="Biarritz"
                                />
                            </div>
                            <div className="row">
                                <div className="col-md-6  ">
                                    <div className="service-main">
                                Биарриц & Берег Басков уделяет особое внимание организации и проведении различных мероприятий
                                , будь то праздник в кругу семьи, корпоративная вечеринка или ужин среди друзей.<br /> 
                                наш персонал реализует мероприятие на ваш выбор, в зависимости от ваших критерий, 
                                и предоставляет <strong>полный комплекс услуг:</strong><br />

                                 

                                - Подбор площадки мероприятия<br />

                                - Резервация залов<br />

                                - Организация питания<br />

                                - Фотограф, технические решения<br />

                                - Подбор персонала и девушек хостесс для мероприятия<br />

                                - Предоставление нужных адресов агентств, 
                                занимающихся организацией и проведением мероприятий 
                                в Биаррице... и во всем регионе Страны Басков.
                        </div>
                        </div>
                                    <div className="col-md-6  ">
                                    <div className="service-main">
                                        <img
                                        src="https://images.ctfassets.net/cr97yccuaymg/47hB8sNfjmsrsd2VeIf6KF/b55d534699cb7dbc6d7ac5381ef06710/meropriyatiye.jpeg?h=250"
                                            alt="Biarritz"
                                        />

                        </div>
                        </div>
                        </div>
                        </div>
                        
                        <div className="col-md-6 ">
                            <div className="service-main">
                                <h2>Водитель :</h2>
                            </div>
                            <div className="service-main">
                                <img
                                    src="https://images.ctfassets.net/cr97yccuaymg/6NLRPOSAIKxfezndODpEYk/7f3c31ceda26ba48dac906522a5e29a0/voditel.jpeg?h=250"
                                    alt="Biarritz"
                                />
                            </div>
                            <div className="service-main">
                               
                                

                                Также вы можете воспользоваться услугами <strong>личного водителя</strong> для деловых поездок или отдыха,
                                наш персонал ответит на любой ваш запрос и позаботится о вашем максимальном комфорте
                                и по вашему прибытию в аэропорту вас будет ждать водитель, 
                                который доставит вас до вашего места проживания 
                                  и останется в вашем распоряжении во время всего пребывания в Стране Басков и во время всех передвижений.<br />

                                 

                                <strong>Вы - владелец автомобиля?</strong>  <br />Мы обеспечим поддержание в рабочем состоянии вашего автомобиля
                                 в ваше отсутствие и гарантируем регулярный контроль для вашего спокойствия.
                        </div>
                        </div>
                        <div className="col-md-6 ">
                            <div className="service-main">
                                <h2>Кейтеринг :</h2>
                            </div>
                            <div className="service-main">
                                <img
                                    src="https://images.ctfassets.net/cr97yccuaymg/4lSLIoibxXadYcfuydOv12/473bf6348485e8d4176dd87edc93e787/keytering.jpeg?h=250"
                                    alt="Biarritz"
                                />
                            </div>
                            <div className="service-main">
                                

                                Удобное расположение в аэропорту Биаррица, а точнее в терминале Деловой Авиации,
                                филиал ON AIR в тесном сотрудничестве с нашим персоналом гарантируют вам 
                                самый качественный сервис на борту самолёта для организации приятного и комфортабельного полёта
                                и предоставляет <strong>следующие услуги:</strong><br />

                                 

                                - Персональный кейтеринг в зависимости от размера и нужд разных самолётов<br />

                                - Блюда из самых элитных ресторанов со звёздами Мишлен<br />

                                - Местные продукты высшего качества: мясные деликатесы, местный сыр, тапасы<br />

                                - Большой выбор кулинарных изысков: закуски, кондитерские изделия, фрукты, ягоды...<br />

                                - Большой выбор напитков: шампанское, вино, сидр, другие...<br />

                                - Приготовление подносов для экипажа<br />

                                - Различные услуги: декорация, цветы, чистка белья самолёта и другое...


                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </Layout>


);

