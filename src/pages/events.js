
import React from "react";

import Layout from "../components/layout";
import SEO from "../components/seo";


export default () => (

    <Layout>
        <SEO
            title="Организация мероприятий"
            keywords={['биарриц', 'Англет', 'страна Басков', 'отдых в Биаррице', 'гид в Биаррице', 'серфинг', 'гольф', 'экскурсии в Биаррице', 'аренда виллы в Биаррице', 'недвижимость в Биаррице']}
        />
        <div className="service section" id="Service">
            <div className="container">
                <div className="container-service">
                <div className="section-head">
                    <h4>Самые интересные события в Биаррице и его окрестностях в 2020-2021 году:</h4>
                </div>

                <div className="row">
                    <div className="col-md-12  ">
                        <div className="service-main">
                            <h2>Рождество и Новый год :</h2>
                            </div>
                            <div className="service-main">
                            <img
                                    src="https://images.ctfassets.net/cr97yccuaymg/5053xqiD6rGY5Z3xDASnAo/4d7bd5d0e014173c79980f6ed6ab2214/22326a_0d02f17c700750ad725ce000d995054d.webp?w=600&h=239&q=50"
                                alt="Biarritz"
                            />
                            </div>
                        <div className="service-main">
                            <strong>30 ноября по 1 января</strong> - Рождественская ярмарка в Байонне <br />
                            <strong>20 декабря по 6 января</strong> - Световое лазерное шоу в Биаррице, <br />
                                 каждый день с 18 до 23 часов на главных зданиях города <br />
                            <strong>21 декабря</strong> - Карнавальное шествие в Байонне с музыкой, танцами <br />
                                 и встреча с  баскским Дедом Морозом - Олентцеро! <br />
                            <strong>7  января - 17 февраля</strong> - Зимние рапродажи
                        </div>
                        </div>
                    <div className="col-md-6 ">
                            <div className="service-main">
                                <h2>Февраль :</h2>
                                </div>
                            <div className="service-main">
                                <img
                                    src="https://images.ctfassets.net/cr97yccuaymg/4cU2p8w3Fznv9EZdd1DxJc/964c7d81ffc151eb2e59127dc5262d76/fev.jpeg?h=250"
                                    alt="Biarritz"
                                />
                            </div>
                        <div className="service-main">
                            <strong>7 января - 12 февраля</strong> - Зимние рапродажи <br />
                            <strong>6-8 февраля</strong>   - Гастрономический и винный салон в Биаррице - Hall d'Iraty  <br />
                            <strong>15-22 февраля</strong> - Карнавалы в Байонне и Сен Жан де Люзе
                        </div>
                        </div>
                    <div className="col-md-6 ">
                            <div className="service-main">
                                <h2>Март :</h2>
                                </div>
                            <div className="service-main">
                                <img
                                    src="https://images.ctfassets.net/cr97yccuaymg/5uty1pHiGGIj02UnDwmIsL/c7b8e91d97c780ad874bb771357b717e/mar.jpeg?h=250"
                                    alt="Biarritz"
                                />
                            </div>
                        <div className="service-main">
                            <strong>4-6 марта</strong>  - Салон недвижимости в Hall d'Iraty <br />
                            <strong>16-22 марта</strong> - Весенняя ресторанная неделя в Биаррице
                        </div>
                        </div>
                    <div className="col-md-6  ">
                            <div className="service-main">
                                <h2>Апрель :</h2>
                                </div>
                            <div className="service-main">
                                <img
                                    src="https://images.ctfassets.net/cr97yccuaymg/2sTgHt7D87W9HNwOPE2tyw/33d85892171b5d4758c19ec6b358f7a6/avr.jpeg?h=250"
                                    alt="Biarritz"
                                />
                            </div>
                        <div className="service-main">
                            <strong>8-13 апреля</strong> - Соревнования по серфингу Maïder Arosteguy <br />
                            <strong>9-12 апреля</strong> - Ярмарка Байоннской ветчины в Байонне <br />
                            <strong>10-13 апреля</strong> - Антикварный салон в Биаррице <br />
                            <strong>20-22 апреля</strong> - Пасха в Биаррице (праздничные пасхальные гуляния, анимация) <br />
                            <strong>24-26 апреля</strong> - Весенние распродажи во многих бутиках Биаррица
                        </div>
                        </div>
                    <div className="col-md-6  ">
                            <div className="service-main">
                                <h2>Май :</h2>
                                </div>
                            <div className="service-main">
                                <img
                                    src="https://images.ctfassets.net/cr97yccuaymg/6r5BtRaiTOxpurhLVtIaIT/fd268aa73f2a08530216753d0412fa52/mai.jpeg?h=250"
                                    alt="Biarritz"
                                />
                            </div>
                        <div className="service-main">
                            <strong>11 мая - 23 июня</strong>  - Выставка, посвященная моде в Биаррице и дизайнерам 20-х гг:
                             Маделен Виоле, Жан Пату, Коко Шанель, Кристобаль Баленсиага  <br />
                             - цокольный этаж церкви Святой Евгении в Биаррице <br />
                            <strong>15-18 мая</strong> - Фестиваль русских фильмов "Кинорама 2020" в кинотеатре Биаррица <br />
                            <strong>15-16 мая</strong> - Шоколадный праздник в шоколадной столице  Байонне <br />
                            <strong>25 мая - 2 июня</strong> - Соревнования по серфингу  ISA World Surfing Games 2019
                        </div>
                        </div>
                    <div className="col-md-6  ">
                            <div className="service-main">
                                <h2>Июнь:</h2>
                                </div>
                            <div className="service-main">
                                <img
                                    src="https://images.ctfassets.net/cr97yccuaymg/5yutzypHV7G6FW1mM0cwgP/2299e164d1f66f6fed30f8efe5b41303/june.jpeg?h=250"
                                    alt="Biarritz"
                                />
                            </div>
                        <div className="service-main">
                            <strong>8-10 июня</strong> - Фестиваль уличного искусства в Биаррице  <br />
                            <strong>4-7 июня</strong> - Фестиваль "Биарриц во времена лихих двадцатых годов" <br />
                            <strong>10-14 июня</strong>- фестиваль Wheels and Waves, около музея Cité de l'Océan <br />
                            <strong>23 июня по 28 июня</strong> - Летний фестиваль "Казетас", пляж Берег Басков
                            <strong>21 июня</strong> - Праздник музыки по всей Франции
                        </div>
                        </div>
                    <div className="col-md-6  ">
                            <div className="service-main">
                                <h2>Праздники в Байонне и коррида:</h2>
                                </div>
                            <div className="service-main">
                                <img
                                    src="https://images.ctfassets.net/cr97yccuaymg/5yXyJMoypMTVuZp36zS0PH/d0ea72c6cc10404d496c6ef8700c83df/july.jpeg?h=250"
                                    alt="Biarritz"
                                />
                            </div>
                        <div className="service-main">
                            Знаменитые праздники в Байонне длятся в течении 5 дней:  <br />
                             карнавалы, концерты, корриды, танцы,  и фейерверки - очень насыщенная программа.
                            Байонна – первый город во Франции, в котором проходили бои быков.  <br />
                            В 1893 году в Байонне была открыта самая большая на юго-западе Франции арена для корриды.

                            Даты проведения :
                            <strong>с 25 по 29 июля</strong>  - Знаменитый летний фестиваль ​​в Байонне
                            <strong>29 июля, 15 августа и с 31  августа по 2 сентября</strong> - Праздничная коррида
                        </div>
                        </div>
                    <div className="col-md-6 ">
                            <div className="service-main">
                            <h2>Фейерверки в Биаррице:</h2>
                            </div>
                            <div className="service-main">
                                <img
                                    src="https://images.ctfassets.net/cr97yccuaymg/7GQfxS5lJf6vjRyfHSxtij/82732ac7735aed727c15bb1d2337aa10/aout.jpeg?h=250"
                                    alt="Biarritz"
                                />
                            </div>
                        <div className="service-main">
                            В июле и августе потрясающие фейерверки украсят вечернее небо Биаррица
                             и знаменитый "пляж королей" или Гранд Пляж, <br /> 
                              в этом году ожидается 2 пиротехнических шоу : <br />

                            <strong>14 июля-23:00</strong> - Гранд пляж <br />
                            <strong>15 августа-22:30</strong> - Гранд пляж
                        </div>
                        </div>
                    <div className="col-md-6  ">
                            <div className="service-main">
                                <h2>Июль :</h2>
                            </div>
                            <div className="service-main">
                                <img
                                    src="https://images.ctfassets.net/cr97yccuaymg/1dlf5qHlJvtbdC1uPAeuQu/14b8ae5b91eab1d0f928c934c914518e/july1.jpeg?h=250"
                                    alt="Biarritz"
                                />
                            </div>
                            <div className="service-main">
                              
                            <strong>13 июля - 31 августа</strong> - Летние сцены - каждый четверг около комплекса океана <br />
                            <strong>12 - 14  июля</strong> - Средневековая ярмарка в Байонне с 10 до 19 часов <br />
                            <strong>10 - 13 июля</strong> -Серфинг кинофестиваль под открытым небом <br />
                             и праздничный салют в 23:00  в  Англете (пляжи Chambre d' Amour, Sable d'Or) <br />
                            <strong>14 июля</strong>- День взятия Бастилии <br />
                            <strong>15, 29 июля</strong> - Ночная ярмарка на рынке Биаррица <br />
                            <strong>16 июля в 21:30</strong> - Мужской хор басков в церкви Св. Евгении <br />
                            <strong>24-26 июля</strong>- Cоревнования по регби Anglet beach rugby - пляж Sables d'Or в Англет
                        </div>
                        </div>
                    <div className="col-md-6  ">
                            <div className="service-main">
                                <h2>Август :</h2>
                            </div>
                            <div className="service-main">
                                <img
                                    src="https://images.ctfassets.net/cr97yccuaymg/56nwmBxlP1P96cCxduXUMz/47de30719c755e102b933dbc792ab915/aout1.jpeg?h=250"
                                    alt="Biarritz"
                                />
                            </div>
                            <div className="service-main">
                            <strong>3-14 августа</strong> - Международный фестиваль класcической музыки в Биаррице <br />
                            <strong>19 августа</strong>- Уникальный фестиваль "Ночной Серфинг", пляж Sable d'Or в Англете<br />
                            <strong>9 и 23 августа</strong> - Праздник в рыбацком порту Биаррица с 18:00 до 00:00<br />
                            <strong>1-11 августа</strong> - Летние сцены в театре Маланден Балет Биарриц<br />
                            <strong>Каждый четверг</strong>  - Концерты, кинотеатр под открытым небом и другая анимация около выставочного комплекса океана с 19:00
                       
                        </div>
                        </div>
                    <div className="col-md-6  ">
                            <div className="service-main">
                            <h2>Сентябрь:</h2>
                            </div>
                            <div className="service-main">
                                <img
                                    src="https://images.ctfassets.net/cr97yccuaymg/6K3D5So3XWXqMFq9mtblrO/0b0724f2ee98a6de7ff399edb438aa55/sep.jpeg?h=250"
                                    alt="Biarritz"
                                />
                            </div>
                            <div className="service-main">  
                            <strong>4-20 сентября</strong> - Музыкальный фестиваль на Баскском побережье <br />
                            <strong>11 - 20 сентября</strong> - Международный фестиваль " Время любить танец" <br />
                            <strong>12-14 сентября</strong> - фестиваль Rat's cup (соревнования по серфингу, стрит-арт, концерты)<br />
                            <strong>13 сентября</strong>  - Соревнования по серфингу среди собак в Ландах<br />
                            <strong>19-20 сентября</strong>- Соревнования по серфингу Surf Marbella Tag Team, пляж Марбелла<br />
                            <strong>19-20 сентября</strong> - Дни национального наследия по всей Франции<br />
                            <strong>26-27 сентября</strong>  - Осенние распродажи в Биаррице<br />
                            <strong>с 28 сентября по 4 октября</strong> - Фестиваль латино-американского кино в Биаррице
                        
                            </div>
                        </div>
                    <div className="col-md-6  ">
                            <div className="service-main">
                                <h2>Октябрь :</h2>
                            </div>
                            <div className="service-main"> 
                                <img
                                    src="https://images.ctfassets.net/cr97yccuaymg/48tOnqxWMaoKRLUYaGunTa/600bc0d80cbb8d166dd55097588ed484/oct.jpeg?h=250"
                                    alt="Biarritz"
                                />
                            </div>
                            <div className="service-main">
                            <strong>29 сентября - 5 октября</strong> - 22-ой Фестиваль латиноамериканского кино в Биаррице. <br />
                            Показ художественных и документальных   фильмов,  выставки, концерты.<br />
                            <strong>до 6 октября</strong> -Quiksilver Pro France -Чемпионат мира по серфингу среди мужчин в Оссегоре<br />
                            <strong>11-12 октября</strong> - Выставка на открытом воздухе "la Brouillarta" на Гранд Пляже в Биаррице<br />
                            <strong>25 - 26 октября</strong> - Праздник знаменитого баскского перчика в деревушке Эспелетт<br />
                            <strong>29-31 октября</strong> - Хэллоуин в Биаррице
                            </div>
                    </div>
                    <div className="col-md-6  ">
                            <div className="service-main">
                                <h2>Ноябрь :</h2>
                            </div>
                            <div className="service-main">
                                <img
                                    src="https://images.ctfassets.net/cr97yccuaymg/1PofnKvurYV5gCyRgHt7FB/0eca8af5b3ce6936aa90d60002424ed7/nov.jpeg?h=250"
                                    alt="Biarritz"
                                />
                                

                            </div>
                            <div className="service-main">
                            <strong>4-11 ноября</strong> - Ресторанная неделя, обеды и ужины по сниженным ценам <br />
                            <strong>30 ноября</strong> - Открытие лыжного сезона в Пиренеях <br />
                            <strong>до 15 сентября</strong> - Выставка "Океаническая впадина или тайна морской бездны" в Выставочном комлексе Океана в Биаррице
                            </div>
                        </div>
                </div>
                </div>
            </div>
        </div>
            


    </Layout>


);

