
import React from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
export default () => (

    <Layout>
        <SEO
            title="Спорт"
            keywords={['биарриц', 'Англет', 'страна Басков', 'отдых в Биаррице', 'гид в Биаррице', 'серфинг', 'гольф', 'экскурсии в Биаррице', 'аренда виллы в Биаррице', 'недвижимость в Биаррице']}
        />
        <div className="service section" id="Service">
            <div className="container">
                <div className="section-head">
                    <h4> Компания<strong>"Биарриц-Берег Басков"</strong> предлагает помощь в организации вашего активного отдыха:</h4>
                    <h4> серфинг, гребля на доске стоя, гольф, велопрогулки вдоль океана, морские прогулки, рыбалка, йога ...</h4>

                </div>
                <div className="row">
                    <div className="col-md-6 mt-2 ">
                        <div className="service-main">
                            <h2>Йога</h2>
                            <img
                                src="https://images.ctfassets.net/cr97yccuaymg/Mt6BF78YUcUuH8YmDBpCO/eb576e3a92518bd105dfe7946b8757e3/yoga.jpeg?h=250"
                                alt="Biarritz"
                            />
                            <div />
                                 Занятия йогой во время вашего отдыха в Биаррице с тренером, которая окончила знаменитую школу йоги Аштанга-Виньяса и другие учебные курсы, с 15 летним опытом работы.
                            </div>
                    </div>
                    <div className="col-md-6 mt-2">
                        <div className="service-main">
                            <h2>Серф</h2>
                            <img
                                src="https://images.ctfassets.net/cr97yccuaymg/2adkPaXvQSWthpb7GitNRf/9f41bd48e570a985e64e556128063a9f/serf.jpeg?h=250"
                                alt="Biarritz"
                            />
                            <div />
                                 Предлагаем для взрослых и детей уроки серфинга с самыми лучшими инструкторами на побережье, индивидуальные занятия или в небольшой группе
                            </div>

                    </div>
                    <div className="col-md-6 mt-2">
                        <div className="service-main">
                            <h2>Дети</h2>
                            <img
                                src="https://images.ctfassets.net/cr97yccuaymg/3dLTluUIfPOj5DIlZBjfFX/0906d485ec793d8aef888c043aeef77b/deti.jpeg?h=250"
                                alt="Biarritz"
                            />
                            <div />
                                 Для маленьких путешественников мы предлагаем разные развлекательные программы, необычные уроки французского, походы в музеи, парки, зоопарки, русскую няню и много интересного!
                            </div>
                    </div>
                    <div className="col-md-6 mt-2">
                        <div className="service-main">
                            <h2>Рыбалка</h2>
                            <img
                                src="https://images.ctfassets.net/cr97yccuaymg/WWoRowjpffG1SRDxBjgbV/8acdf3a5e0b36c7ace683d490392656a/rybalka.jpeg?h=250"
                                alt="Biarritz"
                            />
                            <div />
                                 Океанская рыбалка
                            </div>
                    </div>
                </div>

            </div>
        </div>
    </Layout>


);

