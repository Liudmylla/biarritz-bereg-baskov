import React from "react";
import Img from "gatsby-image";

const Header = ({ data }) => (
  <div className="about section" id="About">
    <div className="container">
      <div className="about-main row">
        <div className="left col-md-5 col-lg-4 ">
           <Img
            fixed={data.photo.fluid}
            objectFit="cover"
            objectPosition="top center"
            
          /> 
        </div>
        <div className="left col-md-7 col-lg-8">
          <div className="about-details">
             <h2 className="name">{data.name}</h2> 
            {/* <h2 className="sub-position">
              {data.designation}.
            </h2>  */}
            <div
              dangerouslySetInnerHTML={{
                __html: data.description.childMarkdownRemark.html
              }}
            />
            <ul className="details">
              {/* <li>
                <strong>Full Name</strong>
                <p>{data.name}</p>
              </li>
              <li>
                <strong>Age</strong>
                <p>{data.age} Years</p>
              </li> */}
              <li>
                <strong>Контакт:</strong>
                {/* <p>{data.location}</p> */}
                <strong>Марьяна Тебенько</strong>
              </li>
              <li>
                <strong>Телефон: </strong>
                <p>+336 63 24 43 55</p>
              </li>
              <li>
                <strong>Эл.почта:</strong>
                <p>
                  <a id="mail" href={`mailto:${data.gmail}`}>{data.gmail}</a>
                </p>
              </li>
            </ul>
            <div className="socials">
              <ul>
                <li>
                  <a
                    className="fab fa-facebook-f"
                    href={data.facebook}
                    target="_blank"
                    rel="noopener noreferrer"
                  ></a>
                </li>
                <li>
                  <a
                    className="fab fa-twitter"
                    href={data.twitter}
                    target="_blank"
                    rel="noopener noreferrer"
                  ></a>
                </li>
                <li>
                  <a
                    className="fab fa-instagram"
                    href={data.instagram}
                    target="_blank"
                    rel="noopener noreferrer"
                  ></a>
                </li>
                {/* <li>
                  <a
                    className="fab fa-linkedin-in"
                    href={data.linkdin}
                    target="_blank"
                    rel="noopener noreferrer"
                  ></a>
                </li> */}
                {/* <li>
                  <a
                    className="fab fa-github"
                    href={data.github}
                    target="_blank"
                    rel="noopener noreferrer"
                  ></a>
                </li> */}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default Header;
