  <!-- AUTO-GENERATED-CONTENT:START (STARTER) -->
  <p align="center">
    <a href="https://biarritz-beregbaskov.netlify.app/">
      <img alt="biarritz" src="https://images.ctfassets.net/cr97yccuaymg/45ylcjbtczpKPMN0YnLZ18/3ab92b1fcdc0a6026b22714fe98d76cf/1.jpeg?h=250" width="50" />
    </a>
  </p>
  <h1 align="center">
    Site for Biarritz Bereg Baskov 
  </h1>
  <img src="https://images.ctfassets.net/cr97yccuaymg/17B1O46wrI5Hpul5ZkS4Eu/8d8173b669b47cabdc8011c041427df5/5.jpeg?h=250" />

 I have used [Gatsby](https://www.gatsbyjs.org/) + [Contenful](https://www.gatsbyjs.org/packages/gatsby-source-contentful/?=Contenful).

### Live Demo:

https://biarritz-beregbaskov.netlify.app/

### Feature:

- Blogs listing with each blog post.
- Contact form with Email notification using formspree.io.
- Photos and Blogs page listing.
- Different types of sections like About, Service, Blogs, Work, Testimonials, Photos, and contact.
- All settings manage from contentful for example Header Menu, Homepage sections, blogs, and photos, etc.
- Social share in blog details pages with comment ( Disqus ).
- PWA



  <!-- AUTO-GENERATED-CONTENT:END -->
